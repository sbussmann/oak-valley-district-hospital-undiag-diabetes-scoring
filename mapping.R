##############################################################################
# Project: Undiagnosed Diabetes Scoring -- Oak Valley
# Date: 10/6/2014
# Author: Sam Bussmann
# Description: Do some mapping
# Notes: 
##############################################################################

library(RColorBrewer)
library(maptools)
library(ggmap)
library(akima)
library(reshape2)
library(gridExtra)


## Do deciling on individuals (previously done using addresses)
b<-10
qqp<-unique(quantile(tog$score,probs=seq.int(0,1, length.out=b+1)))
outp <- cut(tog$score, breaks=qqp, include.lowest=TRUE, labels=as.character(c(10:1)))

### Get lat/long for patients

mapme<-merge(tog,addr[,c("individualid","longitude","latitude")],
             by.x=c("individualid"),by.y=c("individualid"),all.x=T)
temp<-mapme[!is.na(mapme$longitude),c("longitude","latitude")]
deciletemp<-outp[!is.na(mapme$longitude)]
scoretemp<-tog$score[!is.na(mapme$longitude)]
rawdata <- data.frame(temp$longitude, temp$latitude,fac2num(deciletemp),scoretemp)
names(rawdata) <- c("lon", "lat","decile","score")

rawdata_pt<-rawdata[rawdata$decile==1,]

######### Valley

hdf2 <- get_map(location=c(-120.860753+.20,37.759289+.08),zoom=10,maptype="roadmap",color = "bw")

tlatlon1<-data.frame(lng=-120.860753,lat=37.759289)
### Dot Density 

plot1.1<-ggmap(hdf2, extent = "panel") +
  geom_point(aes(x = lon, y = lat), shape=".", data = rawdata_pt, alpha = .25, colour="dodgerblue2") +
  scale_fill_hue(l=95) + 
  geom_text(data=tlatlon1, aes(x=lng,y=lat, label="+", fontface="bold"),size = 5,colour="red3") +
  theme(legend.position = "none", axis.title = element_blank(), text = element_text(size = 12),
        axis.line=element_blank(), 
        axis.text.x=element_blank(), 
        axis.text.y=element_blank(), 
        axis.ticks=element_blank(), 
        axis.ticks.length=unit(0.3, "lines"), 
        axis.ticks.margin=unit(0.5, "lines"), 
        axis.title.x=element_blank(), 
        axis.title.y=element_blank(),
        plot.margin=unit(c(2, 2, 1, 1), "lines")) +
  ggtitle("Dot Density Map - Top Prospects")


pdf("map_CA_V4.pdf")
plot1.1
dev.off()

### Density map of patients in area

plot2.1<-ggmap(hdf2, extent = "device", maprange=FALSE) +
  geom_density2d(aes(x = lon, y = lat), data = rawdata_pt) +
  stat_density2d(data = rawdata_pt, aes(x = lon, y = lat,  fill = ..level.., alpha = ..level.., h=10),
                 size = 0.01, bins = 20, geom = 'polygon') +
  scale_fill_gradient(low = "white", high = "blue") +
  scale_alpha(range = c(0.05, 0.15), guide = FALSE) +
  geom_text(data=tlatlon1, aes(x=lng,y=lat, label="+", fontface="bold"),size = 5,colour="red3") +
  theme(legend.position = "none", axis.title = element_blank(), text = element_text(size = 12),
        axis.line=element_blank(), 
        axis.text.x=element_blank(), 
        axis.text.y=element_blank(), 
        axis.ticks=element_blank(), 
        axis.ticks.length=unit(0.3, "lines"), 
        axis.ticks.margin=unit(0.5, "lines"), 
        axis.title.x=element_blank(), 
        axis.title.y=element_blank(),
        plot.margin=unit(c(.25, .25, .25, .25), "lines")) +
  ggtitle("Density Map of Top Prospects")

pdf("Map_Density_CA.pdf")
plot2.1
dev.off()
